#!/bin/bash

IFS=$'\n'

files="1 2 3 7 12 12e 13 14 17 18e 19 19e 20 21 22 23 24 25 26 27 28 29 30x 31x 32x 33 34 35 36 37 39  52 56 57 58 60 60e  81 83 84 85x 110 111 112 113 114 115 120 121 130 145 150 154 160 170  200 201 202 203 204 206  210 220 226 230 236  250 251 260 261 262 264 280 281 290 291  HVL JVL KPL MEL WRL"
num="$(echo $files | wc -w)"
i=0

while [ "$(ls | wc -l)" -lt "$num" ]; do
    ((i++))
    if [ "$i" -gt 20 ]; then
        echo 'Failed downloading!'
        exit 1
    fi
    wget -w 0.5 $(echo "$files" | tr ' ' '\n' | sort -n | grep -v '^$' | comm -23 - /dev/stderr 2<<<"$(ls | sort -n)" | sed 's@\(.*\)@https://www.metlink.org.nz/api/v1/ServiceLocation/\1@g')
    sleep 2
done
