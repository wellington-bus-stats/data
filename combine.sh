#!/bin/bash

IFS=$'\n'

cd data

{
    echo '{"Date":"'"$(TZ="Pacific/Auckland" date "+%F %R")"'","Data":['
    for i in *
    do
        echo -n '{"Route":"'"$i"'","Locations":' | cat - "$i" /dev/stderr 2<<<$'},'
    done | head -c-2
    echo
    echo ']}'
} >>../data.json
