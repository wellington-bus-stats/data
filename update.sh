#!/bin/bash

set -e

mkdir data public
cd data
../download.sh
cd ..

wget "${1:-https://wellington-bus-stats.gitlab.io/data/alldataorig.json.gz}" -O- >data.json.gz
zcat data.json.gz >data.json

./combine.sh

monthfirstline=$(grep -n Date data.json | grep -F $(date --date="1 month ago" +%F) | head -n1 | cut -d':' -f1 ||:)
mf=${monthfirstline:-1}
echo $mf

gzip -9 -c data.json >public/alldataorig.json.gz
tail -n+$mf data.json | gzip -9 >public/data.json.gz
